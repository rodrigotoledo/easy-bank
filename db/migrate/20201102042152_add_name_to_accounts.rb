class AddNameToAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :accounts, :name, :string, null: false
  end
end
