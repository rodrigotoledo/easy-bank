# Easy Bank Instructions

The Easy Bank is a simple application to demonstrate deposits, transfers and accounts balances.

###### Specific libraries that you must know

* **`rspec`** and **`rspec-rails`** are necessary for tests. This project don't use UnitTests

* **`guard`** and **`guard-rspec`**, these gems give to you great results in `real time` about what you are coding and tests about it. So if you change something, the `rspec` that invoke the test

* **`faker`**, this gem is useful to generate `fake` data for tests and development

* **`sqlite`** is the databatase used for development and test environments, but **`pg`** is for production `heroku`

###### How start the development and test

This project have the unit and integration tests about every code. To let the development with cover of tests the gems **`guard`** and **`simplecov`** have a command to deal with this. Run in your terminal inside the project of course:

```
bundle exec guard
```

If you press `Enter`  automatically will create the folder `coverage` with `index.html` inside in. This file have information about the coverage of your code. 

###### Git and how to deal with issues

Every issue must be create following this pattern

```
git checkout master
git pull origin master
git checkout -b issues/description-of-issue
```

Each branch must have short commits to guarantee an easy way to rollback points. After have issue fixed, he must save in git repository, merge with `development` branch

```
git push origin issues/description-of-issue
git checkout development
git pull origin development
git merge issues/description-of-issue
```

If in the end of the process above everything is ok, tests and code, finally the branch **`master`** must have been merged

```
git checkout master
git pull origin master
git merge development
git push origin master
```

###### The easy way to develop and test

For some reasons the developer needs every time check if database is ok, models, tests. So to make easy for development exist some commands that you can run

Will reset your database with the new data

```
rails db:drop db:create db:migrate db:seed
```