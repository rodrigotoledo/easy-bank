FactoryBot.define do
  factory :account do
    name { Faker::Name.name }
    email { 'client@easy-bank.com' }
    password { 'asdqwe123' }
    password_confirmation { 'asdqwe123' }
  end
end
